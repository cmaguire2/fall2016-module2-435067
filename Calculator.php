<!DOCTYPE html>
<head><title> Calculator </title></head>
<body>
    <h1>Calculator</h1>
    <!--This form creates buttons, radios and number submit fields for the calculator to run-->
    <form action="Calculator.php" method="get">
        First Number: <br>
        <input type = "text" name = "firstNum"> <br>
        <input type="radio" name="sign" value="+"> +<br>
        <input type="radio" name="sign" value="-"> -<br>
        <input type="radio" name="sign" value="*"> *<br>
        <input type="radio" name="sign" value="/"> /<br>
        Second Number: <br>
        <input type = "text" name = "secondNum">
        <input type="submit" value="Submit"><br>
    </form>
    Your Answer is: 
    <?php
        //checks if required input is available to run calculator
        if(isset($_GET["firstNum"]) && isset($_GET["sign"]) && isset($_GET["secondNum"])){
            $firstNum = $_GET["firstNum"];
            $secondNum = $_GET["secondNum"];
            $sign = $_GET["sign"];
            $answer;
            switch($sign)
            {
                case "+":
                    $answer = $firstNum + $secondNum;
                    break;
                case "-":
                    $answer = $firstNum - $secondNum;
                    break;
                case "*":
                    $answer = $firstNum * $secondNum;
                    break;
                case "/":
                    if($secondNum==0){ //this prevents the calculator from dividing by "0."
                        $answer="Invalid Input";
                    }
                    else{
                        $answer = $firstNum / $secondNum;
                    }
                    break;
                default:
                    $answer = "";
            }
            echo $answer;
            
        }
       
    ?>
    
</body>
</html>

